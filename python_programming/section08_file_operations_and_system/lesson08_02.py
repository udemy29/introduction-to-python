﻿from common_func import print_title
import os

print_title('withステートメントでファイルをopenする')

path = os.path.dirname(__file__)

with open(path + '\\test.txt', 'w') as f:
    f.write('Test\n')
