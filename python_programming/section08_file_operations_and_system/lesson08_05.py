﻿from common_func import print_title
import os

print_title('書き込み読み込みモード')


s = """\
AAA
BBB
CCC
DDD
"""

path = os.path.dirname(__file__)

# 'w+'で書き込み+読み込み。ファイルを新規で作り直す。
with open(path + '\\test.txt', 'w+') as f:
    f.write(s)
    f.seek(0)
    print(f.read())
