﻿from common_func import print_title
import os

print_title('ファイルの読み込み')


s = """\
AAA
BBB
CCC
DDD
"""

path = os.path.dirname(__file__)

# with open(path + '\\test.txt', 'w') as f:
#     f.write(s)

with open(path + '\\test.txt', 'r') as f:
    # print(f.read())
    while True:
        chunk = 2
        line = f.read(chunk)
        print(line)
        if not line:
            break
