﻿from common_func import print_title
import os
import string

print_title('テンプレート')

path = os.path.dirname(__file__)

with open(path + '\\design\\email_template.txt') as f:
    # 変数tはwithの外でも利用できる
    t = string.Template(f.read())

contents = t.substitute(name='Mike', contents='How are you?')
print(contents)
