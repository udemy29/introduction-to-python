﻿from common_func import print_title
import os

print_title('ファイルの生成')

path = os.path.dirname(__file__)
f = open(path + '\\test.txt', 'w')
f.write('Test\n')

# printでもファイル出力可能
print('My', 'name', 'is', 'Mike', sep='-', end='!', file=f)

f.close()
