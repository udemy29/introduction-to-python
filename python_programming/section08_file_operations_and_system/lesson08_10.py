﻿from common_func import print_title
import glob
import os
import zipfile

print_title('zipfileの圧縮展開')

path = os.path.dirname(__file__)

with zipfile.ZipFile(path + '\\test.zip', 'w') as z:
    # z.write(path + '\\test_dir')
    # z.write(path + '\\test_dir\\test.txt')

    for f in glob.glob(path + '\\test_dir\\**', recursive=True):
        print(f)
        z.write(f)

with zipfile.ZipFile(path + '\\test.zip', 'r') as z:
    z.extractall(path + 'zzz')
