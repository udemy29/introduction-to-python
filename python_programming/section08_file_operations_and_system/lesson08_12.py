﻿from common_func import print_title
import subprocess

print_title('subprocessでコマンドを実行する')

cmd = 'dir | findstr READ'
r = subprocess.run(cmd, shell=True, check=True)
print('ReturnCode:', r.returncode)
