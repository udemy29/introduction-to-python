﻿from common_func import print_title
import os
import tarfile

print_title('tarfileの圧縮展開')

path = os.path.dirname(__file__)

with tarfile.open(path + '\\test.tar.gz', 'w:gz') as tr:
    tr.add('test_dir')

with tarfile.open(path + '\\test.tar.gz', 'r:gz') as tr:
    tr.extractall(path='test_tar')
    with tr.extractfile(path + '\\test_dir\\sub_dir\\sub_test.txt') as f:
        print(f.read())
