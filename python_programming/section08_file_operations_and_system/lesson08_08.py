﻿from common_func import print_title
import glob
import os
import pathlib
import shutil

print_title('ファイル操作')

path = os.path.dirname(__file__)

# print(os.path.exists(path + '\\test.txt'))
# print(os.path.isfile(path + '\\test.txt'))
# print(os.path.isdir(path + '\\test.txt'))

# os.rename(path + '\\test.txt', path + '\\renamed.txt')

# os.symlink(path + '\\renamed.txt', path + '\\symlink.txt')

# os.mkdir(path + '\\test_dir')
# os.rmdir(path + '\\test_dir')

# pathlib.Path(path + '\\empty.txt').touch()
# os.remove(path + '\\empty.txt')

# os.mkdir(path + '\\test_dir')
# os.mkdir(path + '\\test_dir\\test_dir2')
# print(os.listdir(path + '\\test_dir'))

# pathlib.Path(path + '\\test_dir\\test_dir2\\empty.txt').touch()
# print(glob.glob(path + '\\test_dir\\test_dir2\\*'))

# shutil.copy(path + '\\test_dir\\test_dir2\\empty.txt',
#             path + '\\test_dir\\test_dir2\\empty2.txt')

# shutil.rmtree(path + '\\test_dir')

print(os.getcwd())
