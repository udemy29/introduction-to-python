﻿from common_func import print_title
import os

print_title('seekを使って移動する')


s = """\
AAA
BBB
CCC
DDD
"""

path = os.path.dirname(__file__)

# with open(path + '\\test.txt', 'w') as f:
#     f.write(s)

with open(path + '\\test.txt', 'r') as f:
    print(f.tell())
    print(f.read(1))
    f.seek(5)
    print(f.read(1))
    f.seek(15)
    print(f.read(1))
    f.seek(5)
    print(f.read(1))
