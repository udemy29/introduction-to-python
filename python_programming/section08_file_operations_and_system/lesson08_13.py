﻿import shutil
import os
import time
from common_func import print_title
import datetime

print_title('datetime')

now = datetime.datetime.now()
print(now)
print(now.isoformat())
print(now.strftime('%Y-%m-%d %H:%M'))

today = datetime.date.today()
print(today)

t = datetime.time(hour=1, minute=10, second=5, microsecond=100)
print(t)

d = datetime.timedelta(weeks=1)
print(d)


print('####')
time.sleep(1)
print('####')


file_name = 'test.txt'
if os.path.exists(file_name):
    shutil.copy(file_name, "{}.{}".format(
        file_name, now.strftime('%Y-%m-%d %H:%M:%S')))
with open(file_name, 'w') as f:
    f.write('test')
