"""ロボットと話すためのコントローラー"""
from roboter.models import robot


def talk_about_restaurant():
    """ロボットと話す機能"""
    restaurant_robot = robot.RestaurantRobot()
    restaurant_robot.hello()
    restaurant_robot.recommend_restaurant()
    restaurant_robot.ask_user_favorite()
    restaurant_robot.thank_you()
