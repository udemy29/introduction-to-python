﻿"""CSVに書き込むためのランキングモデルを生成する。

TODO (jsakai) CSVの代わりにDBに書き換える
"""
import collections
import csv
import os
import pathlib


RANKING_COLUMN_NAME = 'NAME'
RANKING_COLUMN_COUNT = 'COUNT'
RANKING_CSV_FILE_PATH = 'ranking.csv'


class CsvModel(object):
    """ベースとなるcsvモデル。"""

    def __init__(self, csv_file):
        self.csv_file = csv_file
        if not os.path.exists(csv_file):
            pathlib.Path(csv_file).touch()


class RankingModel(CsvModel):
    """CSVに書き込むランキングモデルを生成するクラスの定義"""

    def __init__(self, csv_file=None, *args, **kwargs):
        if not csv_file:
            csv_file = self.get_csv_file_path()
        super().__init__(csv_file, *args, **kwargs)
        self.column = [RANKING_COLUMN_NAME, RANKING_COLUMN_COUNT]
        self.data = collections.defaultdict(int)
        self.load_data()

    def get_csv_file_path(self):
        """CSVファイルパスを設定します。

        設定に設定されている場合はCSVパスを使用し、それ以外の場合はデフォルトを使用します
        """
        csv_file_path = None
        try:
            import settings as settings
            if settings.CSV_FILE_PATH:
                csv_file_path = settings.CSV_FILE_PATH
        except ImportError:
            pass

        if not csv_file_path:
            csv_file_path = RANKING_CSV_FILE_PATH
        return csv_file_path

    def load_data(self):
        """CSVデータをロードします。

        Returns:
            dict: DICTタイプのランキングデータを返します。
        """
        with open(self.csv_file, 'r+') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                self.data[row[RANKING_COLUMN_NAME]] = int(
                    row[RANKING_COLUMN_COUNT])
        return self.data

    def save(self):
        """データをCSVファイルに保存します。"""
        # TODO (jsakai) Dead Lockの問題を回避するために、ロックメカニズムを使用してください
        with open(self.csv_file, 'w+', newline='') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=self.column)
            writer.writeheader()

            for name, count in self.data.items():
                writer.writerow({
                    RANKING_COLUMN_NAME: name,
                    RANKING_COLUMN_COUNT: count
                })

    def get_most_popular(self, not_list=None):
        """最もランキングの最上位のデータを取得します。

        Args:
            not_list (list): リストの名前を除外します。

        Returns:
            str: 最もランキングの最上位のデータを返します
        """
        if not_list is None:
            not_list = []

        if not self.data:
            return None

        sorted_data = sorted(self.data, key=self.data.get, reverse=True)
        for name in sorted_data:
            if name in not_list:
                continue
            return name

    def increment(self, name):
        """名前のランクを上げます。"""
        self.data[name.title()] += 1
        self.save()
