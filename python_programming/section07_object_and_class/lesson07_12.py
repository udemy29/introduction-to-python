﻿from common_func import print_title

print_title('クラスメソッドとスタティックメソッド')


class Person(object):
    kind = 'human'

    def __init__(self):
        self.x = 100

    @classmethod
    def what_is_your_kind(cls):
        """クラスメソッド

        Returns:
            str: 種類
        """
        return cls.kind

    @staticmethod
    def about(year):
        """スタティックメソッド

        Args:
            year (int): _description_
        """
        print('about human {}'.format(year))


a = Person()
print(a)
print(a.what_is_your_kind())

# ()がないとインスタンス生成されない
b = Person
print(b)
print(b.what_is_your_kind())

Person.about(100)
