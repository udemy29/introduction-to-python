﻿from common_func import print_title


class Person(object):
    def say_something(self):
        print('hello')


print_title('クラスの定義')

person = Person()
person.say_something()
