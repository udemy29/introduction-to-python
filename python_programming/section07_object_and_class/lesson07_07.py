﻿from common_func import print_title

print_title('クラスを構造体として扱う時の注意点')


class T(object):
    pass


t = T()
t.name = 'Mike'
t.age = 20
# 新しくクラスの変数を定義できるが。。。
t.__enable_auto_run = True
print(t.name, t.age, t.__enable_auto_run)
