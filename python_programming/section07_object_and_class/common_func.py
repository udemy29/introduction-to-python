﻿from termcolor import colored


def print_title(title):
    print(colored(f'\n# {title}\n\n', 'green'))
