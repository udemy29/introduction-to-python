﻿from common_func import print_title

print_title('コンストラクタとデストラクタ')


class Person(object):
    def __init__(self, name):
        """コンストラクタ

        Args:
            name (str): 名前
        """
        self.name = name

    def say_something(self):
        print('I am {}. hello.'.format(self.name))
        self.run(10)

    def run(self, num):
        print('run' * num)

    def __del__(self):
        """デストラクタ
        """
        print('good bye')


person = Person('Mike')
person.say_something()

del person

print('####')
