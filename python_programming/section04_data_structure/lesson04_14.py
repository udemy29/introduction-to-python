﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 集合のメソッド')

s = {1, 2, 3, 4, 5}
print('s', s, sep=': ')

s.add(6)
print('s', s, sep=': ')

s.add(6)
print('s', s, sep=': ')

s.remove(6)
print('s', s, sep=': ')

s.clear()
print('s', s, sep=': ')
