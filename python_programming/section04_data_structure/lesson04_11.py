﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 辞書型のコピー')

x = {'a': 1}
y = x
y['a'] = 1000

print(x)
print(y)


x = {'a': 1}
y = x.copy()
y['a'] = 100

print(x)
print(y)
