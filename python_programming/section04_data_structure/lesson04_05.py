﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# リストの使い方')

seat = []
min = 0
max = 5
print(min <= len(seat) < max)

seat.append('p')
print(min <= len(seat) < max)

seat.append('p')
seat.append('p')
seat.append('p')
seat.append('p')
print(min <= len(seat) < max)

seat.pop()
print(min <= len(seat) < max)
