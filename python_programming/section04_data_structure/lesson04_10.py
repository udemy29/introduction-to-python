﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 辞書型のメソッド')

d = {'x': 10, 'y': 20}
print(d)

print(d.keys())
print(d.values())

d2 = {'x': 1000, 'y': 500, 'j': 500}
d.update(d2)
print(d)

print(d["x"])
print(d.get('x'))

r = d.get('z')
print(type(r))

d.pop('x')
print(d)

del d['y']
print(d)

# del d

d.clear()
d = {'a': 100, "b": 200}
print(d)


print('a' in d)
