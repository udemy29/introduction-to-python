﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 集合型')

a = {1, 2, 2, 3, 4, 4, 4, 5, 6}
print(a)
print(type(a))

b = {2, 3, 3, 6, 7}
print(b)

print(a - b)
print(b - a)
print(a & b)
print(a | b)
print(a ^ b)
