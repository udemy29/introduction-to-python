﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# タプルのアンパッキング')

num_tuple = (10, 20)
print(num_tuple)

x, y = num_tuple
print(x)
print(y)

min, max = 0, 100
print(min, max)
print(type(min))
print(type(max))

a = 100
b = 200
print(a, b)
# 入れ替え
a, b = b, a
print(a, b)
