﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# リスト型')

lst = [1, 20, 4, 50, 2, 1, 2]
print(lst)
print(lst[0])
print(lst[1])
print(lst[-1])
print(lst[-2])
print(lst[0:2])
print(lst[2:])
print(lst[2:])

print(list('abcdefg'))

n = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
a = ['a', 'b', 'c']
x = [n, a]
print(n)
print(x)
print(x[0])
print(x[1][2])
