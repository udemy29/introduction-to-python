﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# タプル型')

t = (1, 2, 3, 4, 1, 2)
print(type(t))

# タプルは代入できない
# t[0] = 100

print(t)
print(t[0])
print(t[-1])
print(t[2:5])
print(t.index(1))
print(t.index(1, 1))
print(t.count(1))
# print(help(tuple))

t = ([1, 2, 3], [4, 5, 6])
print(t)
print(t[0][0])

t = ()
print(type(t))

# １つしかない場合タプルとされない
t = (1)
print(type(t))

# １つしかない場合タプルとされない→カンマをつける
t = (1,)
print(type(t))
