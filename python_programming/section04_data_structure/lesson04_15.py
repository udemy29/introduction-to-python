﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 集合の使いどころ')

my_friends = {'A', 'C', 'D'}
A_friends = {'B', 'D', 'E', 'F'}

print(my_friends & A_friends)


f = ['apple', 'banana', 'apple', 'banana']
kind = set(f)
print(kind)
