﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 辞書の使いどころ')

fruitslist = [
    ['apple', 100],
    ['banana', 200],
    ['orange', 300],
]

for f in fruitslist:
    if f[0] == 'banana':
        print(f[1])


fruits = {
    'apple': 100,
    'banana': 200,
    'orange': 300
}

print(fruits['banana'])
