﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# タプルの使い所')

chose_from_two = ('A', 'B', 'C')

print(chose_from_two)

answer = []
answer.append('A')
answer.append('C')

print(answer)
