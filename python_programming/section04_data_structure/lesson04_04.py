﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# リストのコピー')

i = [1, 2, 3, 4, 5]
j = i
print('i=', i)
print('j=', j)

# 参照渡し(リストやディクショナリ)
j[0] = 100
print('id_i=', id(i))
print('id_j=', id(j))
print('i=', i)
print('j=', j)

# 値渡し
x = [1, 2, 3, 4, 5]
y = x.copy()
y[0] = 100
print('x=', x)
print('y=', y)

# 値渡し（リストやディクショナリ以外）
X = 20
Y = X
Y = 5
print(id(X))
print(id(Y))
print('X=', X)
print('Y=', Y)
