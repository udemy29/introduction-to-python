﻿from operator import truediv


def print_title(title):
    print(f'\n{title}\n\n')


print_title('# リストのメソッド')

r = [1, 2, 3, 4, 5, 1, 2, 3]
# 2のインデックス
print(r.index(2))
# 2のインデックス(3番め以降)
print(r.index(2, 3))

print(r.count(3))

if 5 in r:
    print('exist')

r.sort()
print(r)

r.sort(reverse=True)
print(r)

# もとに戻す
r.reverse()
print(r)

r.reverse()
print(r)

s = 'My name is Mike.'
to_split = s.split(' ')
print(to_split)

x = ' '.join(to_split)
print(x)
