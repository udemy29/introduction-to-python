﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# リストの操作')

s = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print(s)
print(s[0])

s[0] = 'X'
print(s)
print(s[2:5])
s[2:5] = ['C', 'D', 'E']
print(s[2:5])

s[2:5] = []
print(s)

s[:] = []
print(s)


n = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(n)
n.append(100)
print(n)
n.insert(0, 200)
print(n)
n.pop()
print(n)
n.pop(0)
print(n)

del n[0]
print(n)

del n
# print(n)

n = [1, 2, 2, 2, 3]
# はじめの2を削除
n.remove(2)
print(n)
n.remove(2)
n.remove(2)
print(n)
# n.remove(2)

a = [1, 2, 3, 4, 5]
b = [6, 7, 8, 9, 10]
a += b
print(a)

x = [1, 2, 3, 4, 5]
y = [6, 7, 8, 9, 10]
x.extend(y)
print(x)
