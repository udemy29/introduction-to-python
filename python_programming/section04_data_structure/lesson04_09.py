﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 辞書型')

d = {'x': 10, 'y': 20}
print(d)
print(type(d))

print(d['x'])
print(d['y'])

d['x'] = 100
print(d['x'])

d['x'] = 'XXX'
print(d)

d[1] = 10000
print(d)
