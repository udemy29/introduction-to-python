﻿def print_title(title):
    print('\n' + title, end='\n\n')


print_title('# 文字の代入')

print('a is {}'.format('test'))
print('a is {} {} {}'.format(1, 2, 3))
print('a is {2} {1} {0}'.format(1, 2, 3))
print('My name is {0} {1}. Watashi ha {1} {0} desu.'.format('Ichiro', 'Sato'))
print('My name is {given} {family}. Watashi ha {family} {given} desu.'.format(
    given='Ichiro', family='Sato'))

print_title('## Python 3.6以降')
a = 'a'
print(f'a is {a}')

x, y, z = 1, 2, 3
print(f'a is {x}, {y}, {z}')
print(f'a is {z}, {y}, {x}')

name = 'Ichiro'
family = 'Sato'
print(f'My name is {name} {family}. Watashi ha {family} {name}')
