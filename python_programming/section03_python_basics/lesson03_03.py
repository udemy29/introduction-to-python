﻿import math


def print_title(title):
    print('\n' + title, end='\n\n')


print_title('# 数値')

print(2 + 2)
print(5 - 2)
print(6 * 2)
print(8 / 3)
print(8 // 3)
print(8 % 3)
print(5 ** 2)
print(round(3.1415, 2))

result = math.sqrt(25)
print(result)

print(help(math))
