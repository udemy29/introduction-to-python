﻿def print_title(title):
    print("\n" + title)


print_title("# 変数宣言")

print_title("## データ型を指定しなくて良い（動的変数）※ 型をつけることも可能だが。。。")
num = 1
name = 'Mike'
is_ok: bool = True

print(num, type(num))
print(name, type(name))
print(is_ok, type(is_ok))

print_title("## intの変数にstrを設定→strになる。")
num = name
print(num, type(num))

print_title("## 型変換")
str_num = '1'
new_num = int(str_num)
print(new_num, type(new_num))
