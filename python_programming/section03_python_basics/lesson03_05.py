﻿def print_title(title):
    print('\n' + title, end='\n\n')


print_title('# 文字のメソッド')

s = 'My name is Mike. Hi Mike.'
print(s)

print_title('## 指定文字ではじまる？')
is_start = s.startswith('My')
print(is_start)
is_start = s.startswith('X')
print(is_start)

print_title('## 文字検索')
print(s.find('Mike'))
print(s.rfind('Mike'))
print(s.count('Mike'))

print_title('## はじめ大文字、あと小文字')
print(s.capitalize())

print_title('## 単語の始まりを大文字')
print(s.title())

print_title('## 大文字')
print(s.upper())

print_title('## 小文字')
print(s.lower())

print_title('## 置換')
print(s.replace('Mike', 'Nancy'))
