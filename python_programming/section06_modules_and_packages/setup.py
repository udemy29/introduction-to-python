﻿from distutils.core import setup

setup(
    name='python_programming',
    version='1.0.0',
    packages=['lesson_package', 'lesson_package.talk', 'lesson_package.tools'],
    url='https://sample.com/',
    license='MIT',
    author='XXXXX',
    description='Sample package'
)

# python setup.py sdist --formats=gztar
