﻿import collections
import os
import sys

import termcolor

import common_func
from common_func import print_title

print_title('importする際の記述の仕方')

# 順番
# 1. 標準ライブラリ
# 2. サードパーティライブラリ
# 3. チームで利用する共通パッケージ
# 4. ローカルファイル

print(collections.__file__)
print(sys.path)
print(termcolor.__file__)
print(common_func.__file__)
