﻿import sys
from common_func import print_title

print_title('コマンドライン引数')

# コマンド
# python lesson06_01.py option1 option2

for i in sys.argv:
    print(i)
