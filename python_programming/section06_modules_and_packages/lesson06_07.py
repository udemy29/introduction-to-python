﻿from common_func import print_title


print_title('組み込み関数')

# https://docs.python.org/ja/3/library/functions.html

ranking = {
    'A': 100,
    'B': 85,
    'C': 95,
}

print(sorted(ranking, key=ranking.get, reverse=True))
