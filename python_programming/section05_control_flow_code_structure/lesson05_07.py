﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# Noneを判定する場合')

is_empty = None
print(is_empty)

if is_empty is None:
    print('None!')

print(1 == True)
print(1 is True)
