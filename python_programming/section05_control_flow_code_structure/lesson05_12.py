﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# for else文')

for fruit in ['apple', 'banana', 'orange']:
    if fruit == 'banana':
        print('stop eating')
        break
    print(fruit)
else:
    print('I ate all!')
