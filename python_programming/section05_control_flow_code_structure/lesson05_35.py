﻿from common_func import print_title

print_title('独自例外の作成')


class UppercaseError(Exception):
    pass


def check():
    words = ['apple', 'banana', 'ORANGE']
    for word in words:
        if word.isupper():
            raise UppercaseError(word)


try:
    check()
except UppercaseError as ex:
    print('This is my fault. Go next.')
