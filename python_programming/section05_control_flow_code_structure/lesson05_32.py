﻿from common_func import print_title

print_title('ジェネレーター内包表記')


def g():
    for i in range(10):
        yield i


g = g()

g = (i for i in range(10) if i % 2 == 0)
print(type(g))
print(g)

for x in g:
    print(x)
