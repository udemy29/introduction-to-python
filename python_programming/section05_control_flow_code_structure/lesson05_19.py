﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 位置引数とキーワード引数とデフォルト引数')


def menu(entree='beef', drink='wine', dessert='ice'):
    print('entree=', entree)
    print('drink=', drink)
    print('dessert=', dessert)


# menu(entree='beef', dessert='ice', drink='beer')
menu()
menu(entree='chicken', drink='beer')
