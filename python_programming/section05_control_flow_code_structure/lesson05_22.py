﻿from common_func import print_title


print_title('キーワード引数の辞書化')


def menu(**kwargs):
    print(kwargs)
    for k, v in kwargs.items():
        print(k, v, sep=': ')


def menu2(food, *args, **kwargs):
    print(food)
    print(args)
    print(kwargs)


menu(entree='beef', drink='coffee')

d = {
    'entree': 'beef',
    'drink': 'ice coffee',
    'dessert': 'ice'
}

menu(**d)

menu2('banana', 'apple', 'orange', entree='beef', drink='coffee')
