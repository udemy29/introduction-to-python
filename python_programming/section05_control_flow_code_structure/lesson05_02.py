﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 1行が長くなるとき')

# MAX80文字

s = 'aaaaaaaaaaaaaaaa' + 'bbbbbbbbbbbbbbbbbbb'
print(s)

s = 'aaaaaaaaaaaaaaaa' \
    + 'bbbbbbbbbbbbbbbbbbb'
print(s)

x = 1+1+1+1+1 \
    + 1+1+1+1+1
print(x)

x = (1+1+1+1+1
     + 1+1+1+1+1)
print(x)
