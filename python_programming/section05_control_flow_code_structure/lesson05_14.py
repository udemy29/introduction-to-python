﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# enumerate関数')

i = 0
for fruit in ['apple', 'banana', 'orange']:
    print(i, fruit)
    i += 1

for i, fruit in enumerate(['apple', 'banana', 'orange']):
    print(i, fruit)
