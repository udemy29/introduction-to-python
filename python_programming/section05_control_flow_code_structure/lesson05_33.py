﻿from common_func import print_title

print_title('名前空間とスコープ')

animal = 'cat'


def f():
    # global animal
    animal = 'dog'
    print('local:', locals())


f()
print('global:', globals())
