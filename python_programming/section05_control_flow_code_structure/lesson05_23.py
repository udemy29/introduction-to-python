﻿from common_func import print_title


def example_func(param1, param2):
    """Example function with types documented in the docstrings.txt

    Args:
        param1 (int): The first parameter
        param2 (str): The second parameter.

    Returns:
        bool: The return value. True for success, False otherwise.
    """
    print(param1)
    print(param2)
    return True


print_title('Docstringsとは')

print(example_func.__doc__)
help(example_func)
example_func.__doc__