﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 辞書をfor文で処理をする')

d = {'x': 100, 'y': 200}

for k, v in d.items():
    print(k, ':', v)
