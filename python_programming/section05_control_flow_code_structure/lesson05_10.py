﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# While else文')

count = 0
while count < 5:
    if count == 1:
        break
    print(count)
    count += 1
else:
    # breakされた時は通らない
    print('done')
