﻿from common_func import print_title

print_title('例外処理')

l = [1, 2, 3]
i = 5

try:
    l[i]
except IndexError as ex:
    print("Don't worry: {}".format(ex))
except NameError as ex:
    print(ex)
except Exception as ex:
    print('other:{}'.format(ex))
else:
    print('done')
finally:
    print('clean up')
print('last')
