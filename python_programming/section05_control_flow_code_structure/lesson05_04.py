﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 比較演算子と論理演算子')

a = 1
b = 1
print('a', a)
print('b', b)

print('a == b', a == b)
if a == b:
    print('equal')

print('a != b', a != b)
print('a < b', a < b)
print('a > b', a > b)
print('a <= b', a <= b)
print('a >= b', a >= b)
print('a > 0 and b > 0', a > 0 and b > 0)
print('a > 0 or b > 0', a > 0 or b > 0)
