﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# 関数の引数と返り値の宣言')

# あまりしない


def add_num(a: int, b: int) -> int:
    return a + b


result = add_num(10, 20)
print(result)

result = add_num('a', 'b')
print(result)
