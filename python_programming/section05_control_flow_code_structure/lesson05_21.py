﻿from common_func import print_title


print_title('位置引数のタプル化')


def say_something(word, *args):
    print(args)
    print('word=' + word)
    for arg in args:
        print(arg)


say_something('Hi!', 'MIke', 'Nance')

# t = ('Mike', 'Nancy')
# say_something('Hi!', *t)
