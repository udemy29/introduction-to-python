﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# InとNotの使いどころ')

y = [1, 2, 3]
x = 10

if x in y:
    print('in')

if x not in y:
    print('not in')

a = 1
b = 2

if not a == b:
    print('Not equal')

# こちらが推奨
if a != b:
    print('Not equal')

is_ok = True

if is_ok:
    print('hello')

if not is_ok:
    print('not hello')
