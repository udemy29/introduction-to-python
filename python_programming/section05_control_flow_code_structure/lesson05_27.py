﻿from common_func import print_title

print_title('ラムダ')


def change_words(words, func):
    for word in words:
        print(func(word))


# def sample_func(word):
#     return word.capitalize()

def sample_func(word): return word.capitalize()

l = ['Mon', 'true', 'Wed', 'Thu', 'fri', 'sat', 'Sun']
change_words(l, sample_func)
change_words(l, lambda word: word.capitalize())
change_words(l, lambda word: word.lower())
