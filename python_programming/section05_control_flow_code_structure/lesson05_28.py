﻿from common_func import print_title

print_title('ジェネレーター')

l = ['Good morning', 'Good afternoon', 'Good night']

print('##############')

for i in l:
    print(i)

print('##############')


def greeting():
    yield 'Good morning'
    yield 'Good afternoon'
    yield 'Good night'


for g in greeting():
    print(g)

print('##############')

g = greeting()
print(next(g))
print(next(g))
print(next(g))

print('##############')


def counter(num=10):
    for _ in range(num):
        yield 'run'


c = counter()

print(next(c))
