﻿def print_title(title):
    print(f'\n{title}\n\n')


print_title('# range文')

num_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
for i in num_list:
    print(i)

for i in range(10):
    print(i)

print_title('## 2から10(の一つ前)まで')
for i in range(2, 10):
    print(i)

print_title('## 2から10(の一つ前)まで3とばし')
for i in range(2, 10, 3):
    print(i)

print_title('## 10回繰り返す')
for i in range(10):
    print(i, 'hello')

print_title('## 10回繰り返す(iを利用しない)')
for _ in range(10):
    print('hello')
