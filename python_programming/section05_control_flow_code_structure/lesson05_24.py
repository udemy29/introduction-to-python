﻿from common_func import print_title


def outer(a, b):
    def plus(c, d):
        return c+d

    r1 = plus(a, b)
    r2 = plus(b, a)
    print(r1 + r2)


print_title('関数内関数')


outer(1, 2)
